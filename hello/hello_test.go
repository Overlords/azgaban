package hello_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Overlords/azgaban/hello"
)

func TestHello1(t *testing.T) {
	assert.Equal(t, "hello1", hello.Hello1())
}
