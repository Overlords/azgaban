package main

import "fmt"

// Hello send message for hello
func Hello() string {
	return "Hello world!"
}

func main() {
	fmt.Printf("Programm say: %s\n", Hello())
}
